# avisos de conteúdo
- depressão
- morte
- menção a:
	- palavras com cunho sexual
- poesia ruim

---

# viva

recentemente eu me pego me perguntando  
por que é que eu continuo respirando?  
depois de um dia longo e sofrido  
que eu não tenha nem um décimo dos objetivos atingido  
exausta sem nenhuma energia  
pesamentos fazem sua orgia

não é que eu queira a morte  
embora não sofrer tanto assim seria sorte  
um sonho, ainda mais que os outros, distante  
que parece mais provável encontrar da vida o limite  
me parece atrativo  
um livramento convidativo

os sentimentos ruins deixam complicado  
deixando todo o resto completamente acizentado  
cada passo pra frente com o peso mil vezes multiplicado  
cada passo pra trás leve, desembestado  
mas sigo tentando viver  
por que afinal, da muito trabalho morrer.
